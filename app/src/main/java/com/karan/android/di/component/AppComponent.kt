package com.karan.android.di.component

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.karan.android.MainApplication
import com.karan.android.di.AppScope
import com.karan.android.di.module.BinderModule
import com.karan.android.di.module.DatabaseModule
import com.karan.android.di.module.NetworkModule
import com.karan.android.di.module.PrefModule
import com.karan.android.util.SharedPrefsHelper
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@AppScope
@Singleton
@Component(modules = [
    DatabaseModule::class,
    NetworkModule::class,
    BinderModule::class,
    SubComponentsModule::class
])
interface AppComponent {

    fun inject(app: MainApplication)

    fun mainComponent(): MainComponent.Factory

    @Component.Factory
    interface Factory {

        fun create(@BindsInstance app: Application): AppComponent
    }
}