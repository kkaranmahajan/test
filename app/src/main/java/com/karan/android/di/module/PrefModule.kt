package com.karan.android.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides


@Module
object PrefModule {

    @Provides
    @JvmStatic
    fun provideSharedPrefs(application: Application): SharedPreferences? {
        return application.getSharedPreferences("demo-prefs", Context.MODE_PRIVATE)
    }
}