package com.karan.android.di.component

import com.karan.android.di.ActivityScope
import com.karan.android.ui.detail.DetailFragment
import com.karan.android.ui.favorite.FavoriteFragment
import com.karan.android.ui.login.LoginActivity
import com.karan.android.ui.main.MainActivity
import com.karan.android.ui.recipes.RecipesFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent
interface MainComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): MainComponent
    }

    fun inject(loginActivity: LoginActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(recipesFragment: RecipesFragment)
    fun inject(favoriteFragment: FavoriteFragment)
    fun inject(detailFragment: DetailFragment)
}