package com.karan.android.di.component

interface AppComponentProvider {

    fun provideAppComponent(): AppComponent
}