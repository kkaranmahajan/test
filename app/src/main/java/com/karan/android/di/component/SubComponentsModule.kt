package com.karan.android.di.component

import dagger.Module


@Module(subcomponents = [MainComponent::class])
class SubComponentsModule {}