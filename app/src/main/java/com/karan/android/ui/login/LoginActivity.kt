package com.karan.android.ui.login

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.karan.android.LifecycleLoggingActivity
import com.karan.android.MainApplication
import com.karan.android.R
import com.karan.android.databinding.ActivityLoginBinding
import com.karan.android.di.component.MainComponent
import com.karan.android.ui.main.MainActivity
import com.karan.android.util.ActivityUtils
import com.karan.android.util.NetworkStateReceiver
import javax.inject.Inject
import javax.inject.Provider


class LoginActivity : LifecycleLoggingActivity() {


    lateinit var mainComponent: MainComponent
    private lateinit var binding: ActivityLoginBinding
    private var viewModel: LoginViewModel? = null

    @Inject
    lateinit var viewModelProvider: Provider<LoginViewModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        mainComponent = (applicationContext as MainApplication).provideAppComponent().mainComponent().create()

        mainComponent.inject(this)

        viewModel = ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T = viewModelProvider.get() as T
        }).get(LoginViewModel::class.java)
        binding?.viewModel = viewModel

        attachObservers()
    }


    companion object {
        private val TAG = LoginActivity::class.java.name

        fun start(context: Context) {
            context.startActivity(Intent(context, LoginActivity::class.java))
        }
    }


    fun networkAvailable(v: View) {
        if (ActivityUtils.isInternet(this))
            viewModel?.postLoginData()
        else
            ActivityUtils.showInterruptMsg(this, "No Network Available!")
    }


    fun attachObservers() {
        viewModel?.usrsData?.observe(this, androidx.lifecycle.Observer {
            ActivityUtils.showInterruptMsg(this, "Login Successfully")
            MainActivity.start(this)
            finish()
        })

        viewModel?.isLoading?.observe(this, Observer {
            binding.recipesProgressBar.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel?.apiError?.observe(this, Observer {
            ActivityUtils.showInterruptMsg(this, it)
        })

    }
}
