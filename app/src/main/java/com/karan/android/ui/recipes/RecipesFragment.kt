package com.karan.android.ui.recipes

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.karan.android.R
import com.karan.android.data.model.Users
import com.karan.android.databinding.FragmentRecipesBinding
import com.karan.android.ui.main.MainActivity
import com.karan.android.ui.main.OnMainCallback
import com.karan.android.util.ActivityUtils.showInterruptMsg
import com.karan.android.util.NetworkStateReceiver
import javax.inject.Inject
import javax.inject.Provider

/**
 * Display a grid of [Users]s. User can choose to view each recipe.
 *
 */
class RecipesFragment : Fragment(), NetworkStateReceiver.OnNetworkStateReceiverListener,
        OnRecipesFragmentInteractionListener {


    private var mAdapter = RecipesAdapter(this)
    private var mCallback: OnMainCallback? = null
    private var mNetworkReceiver = NetworkStateReceiver()
    private lateinit var binding: FragmentRecipesBinding
    private lateinit var viewModel: RecipeViewModel

    @Inject
    lateinit var viewModelProvider: Provider<RecipeViewModel>


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMainCallback) {
            mCallback = context
        } else {
            throw ClassCastException("$context must implement OnMainCallback!")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)

        (activity as MainActivity).mainComponent.inject(this)

        viewModel = ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T = viewModelProvider.get() as T
        }).get(RecipeViewModel::class.java)


        mNetworkReceiver.addListener(this)
        context?.registerReceiver(mNetworkReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRecipesBinding.inflate(inflater)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        binding.recipesRecyclerView.adapter = mAdapter

        with(viewModel) {
            getMainRecipes()

            recipesData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                mAdapter.users = it
            })

            isLoading.observe(viewLifecycleOwner, Observer {
                binding.recipesProgressBar.visibility = if (it == true) View.VISIBLE else View.GONE
            })

            apiError.observe(viewLifecycleOwner, Observer {
                showInterruptMsg(activity!!, it)
            })


        }

        mAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {

            override fun onChanged() {
                super.onChanged()
                checkEmptyView()
            }
        })

        return binding.root
    }


    override fun showMessage(message: Int) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    override fun gotoDetailPage(users: Users) {
    }

    override fun loadFavorite(users: Users): Users? {
        return viewModel.loadFavorite(users)
    }

    override fun removeFavorite(users: Users) {
        viewModel.deleteFavoriteFromDB(users)
    }

    override fun addOrRemoveFavorites(users: Users) {
        viewModel.addOrRemoveAsFavorite(users)
        mAdapter.notifyDataSetChanged()
    }

    override fun isFavorited(users: Users): Boolean {
        return viewModel.isFavorited(users)
    }

    override fun networkAvailable() {
        viewModel.getMainRecipes()
    }

    override fun networkUnavailable() {
        showInterruptMsg(activity!!, "No Network Available!")
    }

    private fun checkEmptyView() {
        binding.recipesEmptyContainer.visibility = if (mAdapter.itemCount == 0) View.VISIBLE else View.GONE
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.recipes, menu)
    }


    override fun onDestroy() {
        super.onDestroy()
        mNetworkReceiver.removeListener(this)
        unregisterNetworkChanges()
    }

    private fun unregisterNetworkChanges() {
        context?.unregisterReceiver(mNetworkReceiver)
    }


    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    companion object {

        private val TAG = RecipesFragment::class.java.name
    }
}
