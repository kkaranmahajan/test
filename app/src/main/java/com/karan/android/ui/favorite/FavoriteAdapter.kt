package com.karan.android.ui.favorite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.karan.android.R
import com.karan.android.data.model.Users
import com.karan.android.databinding.HolderFavoriteBinding
import com.karan.android.ui.favorite.FavoriteAdapter.FavoriteViewHolder
import kotlin.properties.Delegates

/**
 * [android.support.v7.widget.RecyclerView.Adapter] to adapt
 * Favorite[Users] items into [RecyclerView] via [FavoriteViewHolder]
 *
 */
internal class FavoriteAdapter(private val listener: OnFavoriteFragmentInteractionListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var users: List<Users> by Delegates.observable(emptyList()) { property, oldValue, newValue ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holderFavoriteBinding = HolderFavoriteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FavoriteViewHolder(holderFavoriteBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as FavoriteViewHolder).onBind(getItem(position))
    }

    private fun getItem(position: Int): Users = users[position]

    override fun getItemCount(): Int = users.size


    fun updateData(mUser: Users) {
        if (users.isNotEmpty() && users.contains(mUser)) {
            (users as MutableList<Users>).remove(mUser)
            notifyDataSetChanged()
        }
    }


    inner class FavoriteViewHolder(private val binding: HolderFavoriteBinding) : RecyclerView.ViewHolder(binding.root) {

        fun onBind(users: Users) {

            binding.favoriteTitleTextView.text = users.email

            binding.favoriteImageView.load(users.image) {
                placeholder(R.color.whiteSmoke)
            }

            binding.favoriteIconImageView.setBackgroundResource(if (listener.isFavorited(users)) R.drawable.ic_star_full_vector
            else R.drawable.ic_star_empty_white_vector)

            binding.favoriteIconImageView.setOnClickListener {
                listener.showDeleteFavoriteDialog(users)
            }

            itemView.setOnClickListener {
                listener.gotoDetailPage(users)
            }
        }
    }
}
