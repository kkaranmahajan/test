package com.karan.android.ui.login

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.karan.android.data.model.Users
import com.karan.android.data.model.UsersResponse
import com.karan.android.data.repository.LoginRepository
import com.karan.android.data.source.remote.APIResponse
import com.karan.android.util.ActivityUtils.isEmailValid
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val loginRepository: LoginRepository) : ViewModel() {


    var btnSelected: ObservableBoolean? = null
    var email: ObservableField<String>? = null
    var password: ObservableField<String>? = null

    var usrsData = MutableLiveData<List<Users>>()
    val isLoading = MutableLiveData(false)
    var apiError = MutableLiveData<String>()
    private val compositeDisposable = CompositeDisposable()

    init {
        btnSelected = ObservableBoolean(false)
        email = ObservableField("")
        password = ObservableField("")
    }

    fun onEmailChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        btnSelected?.set(isEmailValid(s.toString()) && password?.get()!!.length >= 5)
    }

    fun onPasswordChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        btnSelected?.set(isEmailValid(email?.get()!!) && s.toString().length >= 5)
    }


    fun postLoginData() {
        setLoading(true)
        loginRepository.postLogin(compositeDisposable, email?.get()!!, password?.get()!!, object : APIResponse<UsersResponse> {
            override fun onSuccess(result: UsersResponse?) {
                setLoading(false)
                usrsData.value = result?.users
            }

            override fun onError(t: Throwable) {
                setLoading(false)
                t.printStackTrace()
                apiError.value = t.message
            }
        })
    }

    fun setLoading(isVisible: Boolean) {
        this.isLoading.value = isVisible
    }


}