package com.karan.android.ui.recipes

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.karan.android.data.model.Users
import com.karan.android.data.model.UsersResponse
import com.karan.android.data.repository.UserRepository
import com.karan.android.data.source.remote.APIResponse
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class RecipeViewModel @Inject constructor(private val userRepository: UserRepository) : ViewModel() {

    val recipesData = MutableLiveData<List<Users>>()
    val isLoading = MutableLiveData(false)
    var apiError = MutableLiveData<String>()
    private val compositeDisposable = CompositeDisposable()


    private fun fetchRecipesData(query: String) {
        setLoading(true)
        userRepository.getRecipes(compositeDisposable, query, object : APIResponse<UsersResponse> {
            override fun onSuccess(result: UsersResponse?) {
                setLoading(false)
                recipesData.value = result?.users
            }

            override fun onError(t: Throwable) {
                setLoading(false)
                t.printStackTrace()
                apiError.value = t.message
            }
        })
    }


    fun getMainRecipes() {
        fetchRecipesData("1")
    }


    fun loadFavoriteItems() {
        val users: MutableList<Users> = userRepository.loadAllFavorites()
        recipesData.value = users
    }

    fun loadFavorite(users: Users): Users? {
        return userRepository.getFavoriteByRecipeId(users)
    }

    fun isFavorited(users: Users): Boolean {
        return loadFavorite(users) != null
    }

    fun deleteFavoriteFromDB(users: Users) {
        userRepository.deleteFavorite(users)
    }

    fun addOrRemoveAsFavorite(users: Users) {
        if (loadFavorite(users) == null) userRepository.insertFavorite(users)
        else userRepository.deleteFavorite(users)
    }

    fun setLoading(isVisible: Boolean) {
        this.isLoading.value = isVisible
    }

    fun loadTags(users: Users): Array<String>? {
        val tags = users.lastName
        if (TextUtils.isEmpty(tags)) {
            return null
        }
        return tags?.split(",".toRegex())?.dropLastWhile { it.isEmpty() }?.toTypedArray()
    }

    fun dispose() {
        if (compositeDisposable.isDisposed) compositeDisposable.dispose()
    }

    companion object {
        private val TAG = RecipeViewModel::class.java.name
    }
}
