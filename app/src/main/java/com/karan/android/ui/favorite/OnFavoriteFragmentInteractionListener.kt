package com.karan.android.ui.favorite

import com.karan.android.data.model.Users

/**
 * To make an interaction between [FavoriteFragment]
 *
 */
interface OnFavoriteFragmentInteractionListener {

    fun gotoDetailPage(users: Users)

    fun showDeleteFavoriteDialog(users: Users)

    fun isFavorited(users: Users): Boolean

}
