package com.karan.android.ui.recipes

import com.karan.android.data.model.Users

/**
 * To make an interaction between [RecipesFragment] and [RecipeViewModel]
 */
interface OnRecipesFragmentInteractionListener {

    fun showMessage(message: Int)

    fun gotoDetailPage(users: Users)

    fun loadFavorite(users: Users): Users?

    fun removeFavorite(users: Users)

    fun addOrRemoveFavorites(users: Users)

    fun isFavorited(users: Users): Boolean

}
