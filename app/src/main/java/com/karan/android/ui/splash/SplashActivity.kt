package com.karan.android.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.karan.android.R
import com.karan.android.databinding.ActivitySplashBinding
import com.karan.android.ui.login.LoginActivity

/**
 * To display the splash screen
 *
 */
class SplashActivity : AppCompatActivity() {

    private val SPLASH_DISPLAY_LENGTH: Long = 1500
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val zoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
        binding.splashImageView.animation = zoomIn
        binding.splashImageView.startAnimation(zoomIn)


        Handler().postDelayed(Runnable {
            LoginActivity.start(this@SplashActivity)
            finish()
        }, SPLASH_DISPLAY_LENGTH)

    }

}
