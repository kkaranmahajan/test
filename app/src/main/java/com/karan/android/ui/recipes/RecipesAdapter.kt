package com.karan.android.ui.recipes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.karan.android.R
import com.karan.android.data.model.Users
import com.karan.android.databinding.HolderRecipeBinding
import com.karan.android.ui.recipes.RecipesAdapter.RecipeViewHolder
import kotlin.properties.Delegates

/**
 * [android.support.v7.widget.RecyclerView.Adapter] to adapt
 * [Users] items into [RecyclerView] via [RecipeViewHolder]
 *
 */
internal class RecipesAdapter(private val listener: OnRecipesFragmentInteractionListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var users: List<Users> by Delegates.observable(emptyList()) { property, oldValue, newValue ->
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holderRecipeBinding = HolderRecipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RecipeViewHolder(holderRecipeBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RecipeViewHolder).onBind(getItem(position))
    }

    private fun getItem(position: Int): Users = users[position]


    override fun getItemCount(): Int = users.size


    inner class RecipeViewHolder(private val binding: HolderRecipeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun onBind(users: Users) {

            binding.recipeTitleTextView.text = users.email

            if (listener.isFavorited(users)) binding.recipeFavoriteImageView.setBackgroundResource(R.drawable.ic_star_full_vector)
            else binding.recipeFavoriteImageView.setBackgroundResource(R.drawable.ic_star_gray_empty_vector)

            binding.recipeFavoriteImageView.setOnClickListener {
                listener.addOrRemoveFavorites(users)
            }
            binding.recipeImageView.load(users.image) {
                placeholder(R.color.whiteSmoke)
            }

            itemView.setOnClickListener {
                listener.gotoDetailPage(users)
            }
        }
    }
}
