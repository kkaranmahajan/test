package com.karan.android.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcelable
import androidx.annotation.NonNull
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
@JsonClass(generateAdapter = true)
data class Users(@Json(name = "id") @NonNull @PrimaryKey var dataId: String = "",
                 @Json(name = "email") var email: String? = null,
                 @Json(name = "avatar") var image: String? = null,
                 @Json(name = "first_name") var firstName: String? = null,
                 @Json(name = "last_name") var lastName: String? = null) : Parcelable