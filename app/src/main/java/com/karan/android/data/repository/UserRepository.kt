package com.karan.android.data.repository

import com.karan.android.data.model.Users
import com.karan.android.data.model.UsersResponse
import com.karan.android.data.source.local.AppDatabase
import com.karan.android.data.source.remote.APIResponse
import com.karan.android.data.source.remote.APIService
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * To handle data operations. It provides a clean API so that the rest of the app can retrieve this data easily.
 * It knows where to get the data from and what API calls to make when data is updated.
 * You can consider repositories to be mediators between different data sources, such as persistent models,
 * web services, and caches.
 */
class UserRepository @Inject constructor(var apiService: APIService, var appDatabase: AppDatabase) {


    fun insertFavorite(users: Users) {
        appDatabase.usersDao.insert(users)
    }

    fun loadAllFavorites(): MutableList<Users> {
        return appDatabase.usersDao.loadAll()
    }

    fun getFavoriteByRecipeId(users: Users): Users? {
        return appDatabase.usersDao.loadOneById(users.dataId)
    }

    fun deleteFavorite(users: Users) {
        appDatabase.usersDao.delete(users)
    }


    fun getRecipes(
            compositeDisposable: io.reactivex.rxjava3.disposables.CompositeDisposable,
            input: String,
            onResponse: APIResponse<UsersResponse>
    ): io.reactivex.rxjava3.disposables.Disposable {
        return apiService.getRecipes(input)
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onResponse.onSuccess(it)
                }, {
                    onResponse.onError(it)
                }).also {
                    compositeDisposable.add(it)
                }

    }

    companion object {

        private val TAG = UserRepository::class.java.simpleName
    }
}
