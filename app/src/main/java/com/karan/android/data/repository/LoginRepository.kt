package com.karan.android.data.repository

import com.karan.android.data.model.UsersResponse
import com.karan.android.data.source.remote.APIResponse
import com.karan.android.data.source.remote.APIService
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import javax.inject.Inject


class LoginRepository @Inject constructor(var apiService: APIService) {


    fun postLogin(
            compositeDisposable: io.reactivex.rxjava3.disposables.CompositeDisposable,
            email: String, password: String,
            onResponse: APIResponse<UsersResponse>
    ): io.reactivex.rxjava3.disposables.Disposable {
        return apiService.login(email, password)
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onResponse.onSuccess(it)
                }, {
                    onResponse.onError(it)
                }).also {
                    compositeDisposable.add(it)
                }

    }

    companion object {

        private val TAG = LoginRepository::class.java.simpleName
    }
}
