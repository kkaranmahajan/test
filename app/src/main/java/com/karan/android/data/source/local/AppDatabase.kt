package com.karan.android.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.karan.android.data.model.Users
import com.karan.android.data.source.local.dao.UsersDao

@Database(entities = [Users::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val usersDao: UsersDao

    companion object {

        val DB_NAME = "KaranDatabase.db"

    }
}
