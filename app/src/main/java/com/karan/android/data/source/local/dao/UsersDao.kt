package com.karan.android.data.source.local.dao

import androidx.room.*
import com.karan.android.data.model.Users


@Dao
interface UsersDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: Users)

    @Query("SELECT * FROM Users")
    fun loadAll(): MutableList<Users>

    @Delete
    fun delete(users: Users)

    @Query("SELECT * FROM Users where dataId = :dataId")
    fun loadOneById(dataId: String): Users?

    @Update
    fun update(users: Users)

}