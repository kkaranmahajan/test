package com.karan.android.data.source.remote


import com.karan.android.data.model.UsersResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.Call
import retrofit2.http.*

/**
 * All Api services must specific in this interface
 *
 */
interface APIService {

    @GET("login")
    fun login(@Query("email") email: String, @Query("password") password: String): Observable<UsersResponse>

    @GET("users")
    fun getRecipes(@Query("page") page: String): Observable<UsersResponse>

}

